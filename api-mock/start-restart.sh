#!/bin/bash


# Try to stop any old instances
echo "Trying to stop old instances"
docker-compose down &> /dev/null

# Start new services
echo "Starting services..."
docker-compose up -d --build --force-recreate
ERR=$?

if [[ $ERR -eq 0 ]]; then
  echo "DONE!"
else
  echo "Some error ocurred while starting services, type 'docker-compose ps' and 'docker-compose logs <exited_service>' to check what happened"
  exit $ERR
fi

if [[ $1 == "-f" ]]; then
  docker-compose logs -f klusters-mock
fi
