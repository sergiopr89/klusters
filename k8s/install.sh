#!/bin/bash

ADDITIONAL_PARAMETERS=""
RELEASE_NAME="klusters"
CHART_PATH="klusters/"

if [[ "${DEBUG,,}" == "true" ]];
then
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --debug --dry-run"
fi

if [[ "${MOCK,,}" == "true" ]];
then
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --set global.mock.enabled=true"
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --set global.mock.suffix=-mock"
fi

if [[ "${CLASSIC_INGRESS_ENABLED,,}" == "true" ]];
then
    # Use standard k8s ingress for non-istio clusters
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --set ingress.enabled=true"
else
    # Get Istio external host IP
    INGRESS_HOST_IP=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')

    if [[ -z $INGRESS_HOST_IP ]]; then
      echo "Can't retrieve ingress gateway IP"
      exit 1
    fi

    INGRESS_HOST_NAME="api.${INGRESS_HOST_IP}.nip.io"
    KEYCLOAK_HOST_NAME="keycloak.${INGRESS_HOST_IP}.nip.io"
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --set ingressgateway.hostname=${INGRESS_HOST_NAME}"
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --set ingressgateway.enabled=true"
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --set keycloak.hostname=${KEYCLOAK_HOST_NAME}"
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --set keycloak.enabled=true"
fi

if [[ ! -z "${NAMESPACE}" ]];
then
    ADDITIONAL_PARAMETERS="${ADDITIONAL_PARAMETERS} --namespace ${NAMESPACE}"
fi

INSTALL_CMD="helm install ${ADDITIONAL_PARAMETERS} --name ${RELEASE_NAME} ${CHART_PATH}"

echo "Installing ${RELEASE_NAME}..."
${INSTALL_CMD}
