FROM python:3.6-alpine

RUN pip install --upgrade pip && pip install pipenv gunicorn && mkdir /app/
ADD . /app/
RUN cd /app && rm -rf .git/ && pipenv install --system
WORKDIR /app/src

EXPOSE 8080
CMD ["gunicorn", "--bind", "0.0.0.0:8080", "wsgi"]
