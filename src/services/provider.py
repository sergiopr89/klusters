from uuid import uuid4
from models.models import KlusterFactory


class KlusterService(object):
    def get_kluster(self) -> object:
        return KlusterFactory.create()

    def fill_kluster(self, payload):
        kluster = self.get_kluster()
        filled_kluster = kluster(
            uuid=payload['uuid'],
            name=payload['name'],
            description=payload['description'],
            endpoint=payload['endpoint'],
            credential=payload['credential'],
            credential_type=payload['credential_type'],
            customer_id=payload['customer_id']
        )
        return filled_kluster

    def save(self, payload: dict) -> (dict, bool):
        payload['uuid'] = uuid4()
        filled_kluster = self.fill_kluster(payload)
        saved = filled_kluster.save()
        return payload, saved

    def update(self, payload: dict) -> bool:
        filled_kluster = self.fill_kluster(payload)
        return filled_kluster.update()

    def find_all(self) -> tuple:
        return self.get_kluster().find_all()

    def find_by_name_and_customer_id(self, name: str, customer_id: str) -> tuple:
        return self.get_kluster().find_by_name_and_customer_id(name, customer_id)

    def find_by_uuid(self, uuid: str) -> tuple:
        return self.get_kluster().find_by_uuid(uuid)

    def exists_by_name_and_customer_id(self, name: str, customer_id: str) -> bool:
        results = self.find_by_name_and_customer_id(name, customer_id)
        if results:
            return True
        return False

    def exists_by_uuid(self, uuid: str) -> bool:
        results = self.find_by_uuid(uuid)
        if results:
            return True
        return False

    def delete_by_uuid(self, uuid: str) -> tuple:
        results = self.get_kluster().delete_by_uuid(uuid)
        return results

    def update_by_name_and_customer_id(self, payload: dict, name: str, customer_id: str) -> tuple:
        filled_kluster = self.fill_kluster(payload)
        return filled_kluster.update_by_name_and_customer_id(name, customer_id)

    def update_by_uuid(self, payload: dict, uuid: str) -> (dict, bool):
        payload['uuid'] = uuid
        filled_kluster = self.fill_kluster(payload)
        return payload, filled_kluster.update_by_uuid(uuid)
