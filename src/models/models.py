import os
from elasticsearch_dsl import Document, Text, Keyword
from elasticsearch_dsl.connections import connections


class ElasticORMKluster(Document):
    uuid = Keyword()
    name = Keyword()
    description = Text()
    endpoint = Text()
    credential = Text()
    credential_type = Text()
    customer_id = Keyword()

    class Index:
        name = 'klusters'
        settings = {
          "number_of_shards": 2,
        }

    @classmethod
    def execute_and_serialize(cls, search):
        search.execute()
        serialized_matches = list()
        for hit in search:
            serialized_matches.append(hit.to_dict())
        return serialized_matches

    @classmethod
    def find_all(cls):
        search = cls.search()
        serialized_matches = cls.execute_and_serialize(search)
        return serialized_matches

    @classmethod
    def find_by_name_and_customer_id(cls, name: str, customer_id: str):
        if name is not None and name != '':
            search = cls.search()\
                .filter('term', name=name)\
                .filter('term', customer_id=customer_id)
        else:
            search = cls.search() \
                .filter('term', customer_id=customer_id)
        serialized_matches = cls.execute_and_serialize(search)
        return serialized_matches

    @classmethod
    def find_by_uuid(cls, uuid: str):
        search = cls.search().filter('term', uuid=uuid)
        serialized_matches = cls.execute_and_serialize(search)
        return serialized_matches

    @classmethod
    def delete_by_name_and_customer_id(cls, name: str, customer_id: str):
        search = cls.search()\
            .filter('term', name=name) \
            .filter('term', customer_id=customer_id)
        serialized_matches = cls.execute_and_serialize(search)
        search.delete()
        return serialized_matches

    @classmethod
    def delete_by_uuid(cls, uuid: str):
        search = cls.search().filter('term', uuid=uuid)
        serialized_matches = cls.execute_and_serialize(search)
        search.delete()
        return serialized_matches

    def update(self, **kwargs):
        search = self.search()\
            .filter('term', name=self.name) \
            .filter('term', customer_id=self.customer_id)
        search.execute()
        for hit in search:
            # Switch instance reference to the found document one
            self.meta.id = hit.meta.id
        # Note that save is created only when the document is new, so just return true
        super(ElasticORMKluster, self).save(**kwargs)
        return True

    def update_by_name_and_customer_id(self, name: str, customer_id: str, **kwargs):
        search = self.search()\
            .filter('term', name=name) \
            .filter('term', customer_id=customer_id)
        search.execute()
        for hit in search:
            # Switch instance reference to the found document one
            self.meta.id = hit.meta.id
            super(ElasticORMKluster, self).save(**kwargs)
            return True
        return False

    def update_by_uuid(self, uuid: str, **kwargs):
        search = self.search().filter('term', uuid=uuid)
        search.execute()
        for hit in search:
            # Switch instance reference to the found document one
            self.meta.id = hit.meta.id
            super(ElasticORMKluster, self).save(**kwargs)
            return True
        return False


class KlusterFactory(object):
    DEFAULT_PERSISTENCE_BACKEND = 'elasticsearch'

    @classmethod
    def _create_elastic_kluster(cls):
        connections.create_connection(hosts=[
            {
                'host': os.getenv('ELASTICSEARCH_HOST', 'localhost'),
                'port': os.getenv('ELASTICSEARCH_PORT', '9200')
            }
        ])
        ElasticORMKluster.init()
        return ElasticORMKluster

    @classmethod
    def create(cls):
        persistence_backend = os.getenv('PERSISTENCE_BACKEND', cls.DEFAULT_PERSISTENCE_BACKEND).lower()
        if persistence_backend == 'elasticsearch':
            kluster = cls._create_elastic_kluster()
        else:
            raise Exception(f'Invalid persistance backend "{persistence_backend}"')
        return kluster
