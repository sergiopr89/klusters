import connexion
from injector import Binder
from flask_injector import FlaskInjector
from connexion.resolver import RestyResolver

from services.provider import KlusterService


def configure(binder: Binder) -> Binder:
    binder.bind(
        KlusterService,
        KlusterService()
    )


app = connexion.FlaskApp(__name__, specification_dir='swagger/')
app.add_api('api-definition.yaml', resolver=RestyResolver('api'))
FlaskInjector(app=app.app, modules=[configure])

if __name__ == '__main__':
    app.run(port=8080)
