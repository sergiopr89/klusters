from injector import inject
from services.provider import KlusterService


class Kluster(object):
    @inject
    def get_by_query(self, kluster_service: KlusterService, name: str, customer_id: str) -> tuple:
        klusters = kluster_service.find_by_name_and_customer_id(name, customer_id)
        if klusters:
            return klusters, 200
        else:
            return klusters, 404

    @inject
    def create_idempotent(self, kluster_service: KlusterService, kluster: dict) -> tuple:
        mapped_kluster, created = kluster_service.save(kluster)
        if created:
            return mapped_kluster, 201
        else:
            return mapped_kluster, 400

    @inject
    def create_idempotent_alias1(self, kluster_service: KlusterService, kluster: dict) -> tuple:
        return self.create_idempotent(kluster_service, kluster)

    @inject
    def get_by_uuid(self, kluster_service: KlusterService, uuid: str) -> tuple:
        kluster = kluster_service.find_by_uuid(uuid)
        if kluster:
            return kluster, 200
        return kluster, 404

    @inject
    def delete_by_uuid(self, kluster_service: KlusterService, uuid: str) -> tuple:
        kluster = kluster_service.delete_by_uuid(uuid)
        if kluster:
            return kluster, 201
        return kluster, 404

    @inject
    def update_by_uuid(self, kluster_service: KlusterService, uuid: str, kluster: dict) -> tuple:
        mapped_kluster, updated = kluster_service.update_by_uuid(kluster, uuid)
        if updated:
            return mapped_kluster, 201
        return mapped_kluster, 404


kluster_instance = Kluster()
